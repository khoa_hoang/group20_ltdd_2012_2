package khoa.moduleevent;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

public class LoadFriendstoListView extends ArrayAdapter {
    ArrayList<FriendsData> data = new ArrayList<FriendsData>();
    Context context;
    int layoutID;
    LoadFriendstoListView(Context context, int layoutId,ArrayList<FriendsData> arrLst)
    {
        super(context, R.layout.friend_layout,arrLst);
        this.layoutID = layoutId;
        this.context = context;
        this.data = arrLst;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View row = inflater.inflate(this.layoutID, null);
        if(data.size() > 0 && position >= 0)
        {
            TextView name_txt = (TextView) row.findViewById(R.id.name);
            TextView phone_txt = (TextView) row.findViewById(R.id.phone);
            TextView address_txt = (TextView) row.findViewById(R.id.address);

            name_txt.setText(data.get(position).sName);
            phone_txt.setText(data.get(position).sPhone);
            address_txt.setText(data.get(position).sAddress);
        }
        return row;
    }
}
class FriendsData implements Serializable
{
    public String sName;
    public String sEmail;
    public String sPhone;
    public String sAddress;
    boolean selected = false;
    FriendsData()
    {

    }

    FriendsData(String name, String email, String phone, String address)
    {
        sName = name;
        sEmail = email;
        sPhone = phone;
        sAddress = address;
    }
    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}