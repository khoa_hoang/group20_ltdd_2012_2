package khoa.moduleevent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class TagFriendsActivity extends Activity {
    ListView lst_friend;
    FriendsCustomAdapter friendsCustomAdapter = null;
    ArrayList<FriendsData> data = new ArrayList<FriendsData>();
    Button btn_OK;

    public static final int RESULT_CODE_TAG_FRIENDS = 103;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tag_friends_layout);
        lst_friend = (ListView) findViewById(R.id.list);
        btn_OK = (Button) findViewById(R.id.ok_btn);

        LoadData();
        friendsCustomAdapter = new FriendsCustomAdapter(this,R.layout.list_friend_layout,data);
        lst_friend.setAdapter(friendsCustomAdapter);

        btn_OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToMain(RESULT_CODE_TAG_FRIENDS);
            }
        });
    }
    private void sendToMain(int resultcode)
    {
        Intent intent = getIntent();

        ArrayList<String> rsdata = new ArrayList<String>();
        String rs = "";
        int n = data.size();
        for (int i = 0; i < n; i++) {
            if(data.get(i).isSelected())
            {
                String [] name = data.get(i).sName.split(": ");
                rsdata.add(name[1]);
            }
        }
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("arrFriends", rsdata);
        intent.putExtra("tagFriends",bundle);

        setResult(resultcode,intent);
        finish();
    }
    private class FriendsCustomAdapter extends ArrayAdapter<FriendsData> {

        private ArrayList<FriendsData> friendsList;

        public FriendsCustomAdapter(Context context, int textViewResourceId,ArrayList<FriendsData> frList) {
            super(context, textViewResourceId, frList);
            this.friendsList = new ArrayList<FriendsData>();
            this.friendsList = frList;
        }

        private class ViewHolder {
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.list_friend_layout, null);

                holder = new ViewHolder();
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;
                        FriendsData frData = (FriendsData) cb.getTag();
                        frData.setSelected(cb.isChecked());
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            FriendsData friendsData = friendsList.get(position);
            holder.name.setText(friendsData.sName);
            holder.name.setChecked(friendsData.isSelected());
            holder.name.setTag(friendsData);

            return convertView;
        }
    }
    private void LoadData() {
        int friendnumberOfProp = 3;
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(this, "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return ;
                }
            }
            myFile = new File(newFolder + "/Friends.txt");
            if (myFile.exists()) {
                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
                String DataRow = "";
                int i = -1;
                FriendsData fdt = new FriendsData();
                while ((DataRow = bufferedReader.readLine()) != null)
                {
                    i++;
                    switch ((i % friendnumberOfProp))
                    {
                        case 0:
                            fdt.sName = DataRow;
                            break;
                        case 1:
                            fdt.sPhone = DataRow;
                            break;
                        default:
                            fdt.sAddress = DataRow;
                            data.add(fdt);
                            fdt = new FriendsData();
                    }
                }
                bufferedReader.close();
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Không thể mở file",Toast.LENGTH_LONG).show();
        }
    }
}
