package khoa.moduleevent;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.GregorianCalendar;


public class AddEventActivity extends Activity{

    EditText edTitle;
    EditText edInfo;
    DatePicker dpDate;
    DatePicker dpDate_Remind;
    TimePicker tpTime_remind;
    EditText edLocation;
    Button btn_OK;
    Button btn_Cancel;
    Button btn_TagFriends;
    TextView tvfriends;

    ArrayList<FriendsData> data = new ArrayList<FriendsData>();

    User mainUser;

    public static final int RESULT_CODE_SAVE_EVENT = 101;
    public static final int RESULT_CODE_SAVE_EVENT_CANCEL = 102;
    public static final int RESULT_CODE_TAG_FRIENDS = 103;
    public static final int REQUEST_CODE_TAG_FRIENDS = 201;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_event);

        edTitle = (EditText) findViewById(R.id.title);
        edInfo = (EditText) findViewById(R.id.info);
        dpDate = (DatePicker) findViewById(R.id.date);
        dpDate_Remind = (DatePicker) findViewById(R.id.date_remind);
        tpTime_remind = (TimePicker) findViewById(R.id.time_remind);
        edLocation = (EditText) findViewById(R.id.location);
        btn_OK = (Button) findViewById(R.id.ok_btn);
        btn_Cancel = (Button) findViewById(R.id.cancel_btn);
        btn_TagFriends = (Button) findViewById(R.id.tag_friend);
        tvfriends = (TextView) findViewById(R.id.friends);

        mainUser = new User();
        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("UserPacket");
        mainUser = (User)packageFromCaller.getSerializable("mainUser");
        LoadData();

        btn_OK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<String> para = new ArrayList<String>();
                para.add(edTitle.getText().toString());
                para.add(edInfo.getText().toString());
                para.add(getStringDateFormat(dpDate));
                para.add(getStringTimeFormat(tpTime_remind));
                para.add(edLocation.getText().toString());
                para.add(mainUser.sName);
                para.add(tvfriends.getText().toString());
                new AddEventAsyncTask().execute(para);
            }
        });

        btn_TagFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent(AddEventActivity.this, TagFriendsActivity.class);
                startActivityForResult(myIntent,REQUEST_CODE_TAG_FRIENDS);
            }
        });

        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToMain(RESULT_CODE_SAVE_EVENT_CANCEL,null);
            }
        });
    }
    public class AddEventAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        private final ProgressDialog dialog = new ProgressDialog(AddEventActivity.this);

        @Override
        protected String doInBackground(ArrayList<String>... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            String res = "-1";
            res = CS.addEvent(params[0].get(0),params[0].get(1),params[0].get(2),params[0].get(3),params[0].get(4),params[0].get(5),params[0].get(6));
            return res;
        }
        @Override
        protected void onPostExecute(String res)
        {
            super.onPostExecute(res);
            dialog.dismiss();

            if(!res.equals("-1"))
            {
                SaveEvent(res);
            }
            else
            {
                Toast.makeText(getApplicationContext(),res,Toast.LENGTH_LONG).show();
            }
        }
    }
    private void SaveEvent(String event_id)
    {
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_SAVE_EVENT_CANCEL,null);
                    return;
                }
            }
            myFile = new File(newFolder + "/EventManager.txt");
            if (!myFile.exists()) {
                boolean res = myFile.createNewFile();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_SAVE_EVENT_CANCEL, null);
                    return;
                }
            }
            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile, true));
            String sTitle = edTitle.getText().toString();
            String sInfo = edInfo.getText().toString();
            String sLocation = edLocation.getText().toString();
            String data = "Event id: " + event_id+ "\n"
                    + "Title: " + sTitle + "\n"
                    + "Information: " + sInfo + "\n"
                    + "Date: " + getStringDateFormat(dpDate) + "\n"
                    + "Date remind: " + getStringDateFormat(dpDate_Remind) + "\n"
                    + "Time remind: " + getStringTimeFormat(tpTime_remind) + "\n"
                    + "Location: " + sLocation + "\n"
                    + "FriendsTag: " + tvfriends.getText() + "\n";
            Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
            buf.append(data);
            buf.close();
            Toast.makeText(getApplicationContext(), "Tạo sự kiện thành công!", Toast.LENGTH_LONG).show();
            EventData eventData = new EventData("Title: " + sTitle,"Information: " + sInfo,"Date: " + getStringDateFormat(dpDate),"Date remind: " + getStringDateFormat(dpDate_Remind),"Time remind: " + getStringTimeFormat(tpTime_remind),"Location: " + sLocation);
            eventData.sEventId = "Event id: " + event_id;
            eventData.sCreater = "FriendsTag: " + mainUser.sName;
            sendToMain(RESULT_CODE_SAVE_EVENT,eventData);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Không thể mở file", Toast.LENGTH_LONG).show();
            sendToMain(RESULT_CODE_SAVE_EVENT_CANCEL,null);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_CODE_TAG_FRIENDS)
        {
            switch (resultCode)
            {
                case RESULT_CODE_TAG_FRIENDS:
                {
                    Bundle bundle = intent.getBundleExtra("tagFriends");
                    ArrayList<String> tagFriend = (ArrayList<String>)bundle.getSerializable("arrFriends");
                    String rs = "";
                    int n = tagFriend.size();
                    for (int i = 0; i < n-1; i++) {
                        rs += tagFriend.get(i)+",";
                    }
                    rs += tagFriend.get(n-1);
                    tvfriends.setText(rs);
                    break;
                }
            }
        }
    }

    private void LoadData() {
        int friendnumberOfProp = 3;
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(this, "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return ;
                }
            }
            myFile = new File(newFolder + "/Friends.txt");
            if (myFile.exists()) {
                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
                String DataRow = "";
                int i = -1;
                FriendsData fdt = new FriendsData();
                while ((DataRow = bufferedReader.readLine()) != null)
                {
                    i++;
                    switch ((i % friendnumberOfProp))
                    {
                        case 0:
                            fdt.sName = DataRow;
                            break;
                        case 1:
                            fdt.sPhone = DataRow;
                            break;
                        default:
                            fdt.sAddress = DataRow;
                            data.add(fdt);
                            fdt = new FriendsData();
                    }
                }
                bufferedReader.close();
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Không thể mở file",Toast.LENGTH_LONG).show();
        }
    }
    private void sendToMain(int resultcode, EventData eventData)
    {
        Intent intent = getIntent();
        if(eventData != null)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("newEvent",eventData);
            intent.putExtra("EventPacket",bundle);
        }
        setResult(resultcode,intent);
        finish();
    }
    private String getStringDateFormat(DatePicker dp)
    {
        int   day  = dp.getDayOfMonth();
        int   month= dp.getMonth();
        int   year = dp.getYear();
        return Integer.toString(day) + "/" + Integer.toString(month) + "/" + Integer.toString(year);
    }
    private String getStringTimeFormat(TimePicker dp)
    {
        int   hours  = dp.getCurrentHour();
        int   minutes= dp.getCurrentMinute();
        return Integer.toString(hours) + "/" + Integer.toString(minutes);
    }
}



