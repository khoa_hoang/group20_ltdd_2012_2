package khoa.moduleevent;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;

public class CallSoapWebService {

    String WDSL_TARGET_NAMESPACE ="http://tempuri.org/";
    String SOAP_ADDRESS = "http://eventmanager.somee.com/eventmanager.asmx";

    public String deleteEvent(String username, String event_id)
    {
        Boolean res = false;
        String SOAP_ACTION= "http://tempuri.org/deleteEvent";
        String OPERATION_NAME = "deleteEvent";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(username);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("event_id");
        PI.setValue(event_id);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
            res = Boolean.parseBoolean(response.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "exception";
        }
        return res.toString();
    }
    public ArrayList<String> checkDeleteEvent(String username)
    {
        ArrayList<String> arrEventIDs = new ArrayList<String>();
        String SOAP_ACTION= "http://tempuri.org/checkDeleteEvent";
        String OPERATION_NAME = "checkDeleteEvent";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(username);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapObject response=(SoapObject) envelope.getResponse();
            int n = response.getPropertyCount();

            for(int i=0; i<n; i++)
            {
                EventData eventsData = new EventData();

                SoapObject soapItem =(SoapObject) response.getProperty(i);

                eventsData.sEventId = soapItem.getProperty("event_id").toString();

                arrEventIDs.add(eventsData.sEventId);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return arrEventIDs;
    }
    public ArrayList<EventData> checkEvent(String username)
    {
        ArrayList<EventData> arrEvents = new ArrayList<EventData>();
        String SOAP_ACTION= "http://tempuri.org/checkNewEvent";
        String OPERATION_NAME = "checkNewEvent";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(username);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapObject response=(SoapObject) envelope.getResponse();
            int n = response.getPropertyCount();

            for(int i=0; i<n; i++)
            {
                EventData eventsData = new EventData();

                SoapObject soapItem =(SoapObject) response.getProperty(i);

                eventsData.sEventId = soapItem.getProperty("event_id").toString();
                eventsData.sTitle = soapItem.getProperty("title").toString();
                eventsData.sInfo = soapItem.getProperty("information").toString();
                eventsData.sDate = soapItem.getProperty("date").toString();
                eventsData.sRemind_time = soapItem.getProperty("time").toString();
                eventsData.sLocation = soapItem.getProperty("location").toString();
                eventsData.sCreater = soapItem.getProperty("creater").toString();

                eventsData.sRemind_date = eventsData.sDate;
                arrEvents.add(eventsData);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return arrEvents;
    }
    public String addEvent(String title, String information, String date, String time, String location, String creator, String friendtags)
    {
        String res = "-1";
        String SOAP_ACTION= "http://tempuri.org/addEvent";
        String OPERATION_NAME = "addEvent";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("title");
        PI.setValue(title);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("information");
        PI.setValue(information);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("date");
        PI.setValue(date);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("time");
        PI.setValue(time);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("location");
        PI.setValue(location);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("creator");
        PI.setValue(creator);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("friendtags");
        PI.setValue(friendtags);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
            res = response.toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "exception";
        }

        return res;
    }
    public ArrayList<FriendsData> checkFriend(String username)
    {
        ArrayList<FriendsData> arrFriends = new ArrayList<FriendsData>();
        String SOAP_ACTION= "http://tempuri.org/checkFriend";
        String OPERATION_NAME = "checkFriend";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(username);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapObject response=(SoapObject) envelope.getResponse();
            int n = response.getPropertyCount();
            for(int i=0; i<n; i++)
            {
                FriendsData friendsData = new FriendsData();

                SoapObject soapItem =(SoapObject) response.getProperty(i);

                friendsData.sName = soapItem.getProperty("user_name").toString();
                friendsData.sAddress = soapItem.getProperty("address").toString();
                friendsData.sPhone = soapItem.getProperty("phone").toString();

                arrFriends.add(friendsData);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
        return arrFriends;
    }
    public String AddFriend(String info)
    {
        Boolean res = false;
        String SOAP_ACTION= "http://tempuri.org/addFriend";
        String OPERATION_NAME = "addFriend";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("information");
        PI.setValue(info);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
            res = Boolean.parseBoolean(response.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "exception";
        }
        return res.toString();
    }
    public Boolean EditUserInfo(User us)
    {
        Boolean res = false;
        String SOAP_ACTION= "http://tempuri.org/editInfo";
        String OPERATION_NAME = "editInfo";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(us.sName);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("address");
        PI.setValue(us.sAddress);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("phone");
        PI.setValue(us.sPhone);
        PI.setType(String.class);
        request.addProperty(PI);

        PI = new PropertyInfo();
        PI.setName("password");
        PI.setValue(us.sPassword);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
            res = Boolean.parseBoolean(response.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        return res;
    }
    public String RegisAccount(User us)
    {

        Boolean res = true;
        String SOAP_ACTION= "http://tempuri.org/addAccount";
        String OPERATION_NAME = "addAccount";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(us.sName);
        PI.setType(String.class);
        request.addProperty(PI);

        PropertyInfo PI1 = new PropertyInfo();
        PI1.setName("address");
        PI1.setValue(us.sAddress);
        PI1.setType(String.class);
        request.addProperty(PI1);

        PropertyInfo PI2 = new PropertyInfo();
        PI2.setName("phone");
        PI2.setValue(us.sPhone);
        PI2.setType(String.class);
        request.addProperty(PI2);

        PropertyInfo PI3 = new PropertyInfo();
        PI3.setName("password");
        PI3.setValue(us.sPassword);
        PI3.setType(String.class);
        request.addProperty(PI3);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapPrimitive response=(SoapPrimitive) envelope.getResponse();
            res = Boolean.parseBoolean(response.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "exception";
        }
        if(res)
            return "ok";
        else
            return "fail";
    }
    public FriendsData GetFriend(String name)
    {
        FriendsData fdata = new FriendsData();

        String SOAP_ACTION= "http://tempuri.org/findFriend";
        String OPERATION_NAME = "findFriend";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(name);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapObject result=(SoapObject)envelope.getResponse();
            fdata.sName = result.getProperty("user_name").toString();
            fdata.sAddress = result.getProperty("address").toString();
            fdata.sPhone = result.getProperty("phone").toString();
        }
        catch (Exception e)
        {
            fdata.sName = null;
            e.printStackTrace();
        }
        return fdata;
    }
}
