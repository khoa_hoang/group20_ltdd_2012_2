package khoa.moduleevent;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;

public class UserActivity extends Activity{

    TextView tvName;
    EditText edAddress;
    EditText edPhone;
    EditText edPassword;
    Button edit_btn;
    Button cancel_btn;
    User mainUser;

    public static final int RESULT_CODE_CANCEL=102;
    public static final int RESULT_CODE_EDIT_USER_INFO_SUCCESS=104;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_management);

        tvName = (TextView)findViewById(R.id.user_name);
        edAddress = (EditText)findViewById(R.id.address);
        edPhone = (EditText)findViewById(R.id.phone);
        edit_btn = (Button)findViewById(R.id.edit_btn);
        edPassword = (EditText)findViewById(R.id.password);
        cancel_btn = (Button)findViewById(R.id.cancel_btn);

        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("UserPacket");
        mainUser = (User)packageFromCaller.getSerializable("mainUser");


        tvName.setText(mainUser.sName);
        edAddress.setText(mainUser.sAddress);
        edPhone.setText(mainUser.sPhone);

        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainUser.sAddress = edAddress.getText().toString();
                mainUser.sPhone = edPhone.getText().toString();
                mainUser.sPassword = edPassword.getText().toString();
                new EditUserInfoAsyncTask().execute(mainUser);
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToMain(RESULT_CODE_CANCEL,null);
            }
        });
    }
    public class EditUserInfoAsyncTask extends AsyncTask<User, Void, Boolean>
    {
        private final ProgressDialog dialog = new ProgressDialog(UserActivity.this);

        @Override
        protected Boolean doInBackground(User... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            Boolean res = CS.EditUserInfo(params[0]);
            return res;
        }
        @Override
        protected void onPostExecute(Boolean result)
        {
            super.onPostExecute(result);
            dialog.dismiss();
            if(result)
            {
                SaveUser();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Cập nhật tài khoản thất bại",Toast.LENGTH_LONG).show();
                sendToMain(RESULT_CODE_CANCEL,null);
            }
        }
    }
    private void SaveUser()
    {
        try
        {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null);
                    return;
                }
            }
            myFile = new File(newFolder + "/User.txt");
            if(myFile.exists())
                myFile.delete();
            boolean res = myFile.createNewFile();
            if (!res) {
                Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                sendToMain(RESULT_CODE_CANCEL,null);
                return;
            }

            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile, true));
            String data = "Name: " + mainUser.sName + "\n"
                    + "Address: " + mainUser.sAddress + "\n"
                    + "Phone: " + mainUser.sPhone + "\n";
            Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
            buf.append(data);
            buf.close();
            Toast.makeText(getApplicationContext(),"Tài khoản " + mainUser.sName + " đã được cập nhật",Toast.LENGTH_LONG).show();
            sendToMain(RESULT_CODE_EDIT_USER_INFO_SUCCESS,mainUser);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Cập nhật tài khoản thất bại",Toast.LENGTH_LONG).show();
            finish();
        }
    }
    private void sendToMain(int resultcode, User us)
    {
        Intent intent = getIntent();
        if(us != null)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("infoUser",us);
            intent.putExtra("UserPacket",bundle);
        }
        setResult(resultcode,intent);
        finish();
    }
}
class User implements Serializable
{
    public String sName;
    public String sPhone;
    public String sAddress;
    public String sPassword;
}