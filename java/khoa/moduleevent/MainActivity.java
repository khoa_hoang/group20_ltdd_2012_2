package khoa.moduleevent;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.CalendarContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class MainActivity extends ActionBarActivity {

    ListView lstv = null;
    ArrayList<EventData> data = new ArrayList<EventData>();
    LoadDatatoListView customAdapter = null;
    int numberOfProp = 8;

    User mainUser;

    public static final int REQUEST_CODE_INPUT_ADD_EVENT = 1;
    public static final int REQUEST_CODE_INPUT_EDIT_REMOVE = 2;
    public static final int REQUEST_CODE_MANAGE_FRIENDS = 3;
    public static final int RESULT_CODE_SAVE_EVENT=101;
    public static final int RESULT_CODE_CANCEL=102;
    public static final int RESULT_CODE_EDIT_EVENT=103;
    public static final int RESULT_CODE_DELETE_EVENT=104;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainUser = new User();
        LoadUser();
        LoadData();
        if(mainUser != null)
        {
            CheckEvent();
            CheckFriend();
        }

        lstv = (ListView) findViewById(R.id.list);
        customAdapter = new LoadDatatoListView(this,R.layout.event_layout,data);
        lstv.setAdapter(customAdapter);
        lstv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(MainActivity.this, EditDeleteEventActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putSerializable("mainUser", mainUser);
                bundle.putSerializable("editedEvent", data.get(position));
                myIntent.putExtra("dataPacket", bundle);
                startActivityForResult(myIntent, REQUEST_CODE_INPUT_EDIT_REMOVE);
            }
        });
    }

    private void CheckEvent()
    {
        new CheckEventAsyncTask().execute(mainUser.sName);
    }
    public class CheckEventAsyncTask extends AsyncTask<String, Void, ArrayList<EventData>>
    {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected ArrayList<EventData> doInBackground(String... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            ArrayList<EventData> arrEvent = new ArrayList<EventData>();
            arrEvent = CS.checkEvent(params[0]);
            return arrEvent;
        }
        @Override
        protected void onPostExecute(ArrayList<EventData> result)
        {
            super.onPostExecute(result);
            dialog.dismiss();
            AddNewEvents(result);
        }
    }
    private void AddNewEvents(ArrayList<EventData> arrEvents)
    {
        for(EventData evtdata : arrEvents)
        {
            SaveEvent(evtdata);
            String temp = evtdata.sDate;
            evtdata.sTitle = "Title: " + evtdata.sTitle;
            evtdata.sInfo = "Information: " + evtdata.sInfo;
            evtdata.sDate = "Date: " + temp;
            evtdata.sLocation = "Location: " + evtdata.sLocation;
            evtdata.sRemind_date = "Date remind: " + temp;
            evtdata.sRemind_time = "Time remind: " + evtdata.sRemind_time;
            data.add(evtdata);
            customAdapter.notifyDataSetChanged();
        }
    }
    private void SaveEvent(EventData evt)
    {
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            myFile = new File(newFolder + "/EventManager.txt");
            if (!myFile.exists()) {
                boolean res = myFile.createNewFile();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile, true));

            String data = "Event id: " + evt.sEventId+ "\n"
                    + "Title: " + evt.sTitle + "\n"
                    + "Information: " + evt.sInfo + "\n"
                    + "Date: " + evt.sDate + "\n"
                    + "Date remind: " + evt.sDate + "\n"
                    + "Time remind: " + evt.sRemind_time + "\n"
                    + "Location: " + evt.sLocation + "\n"
                    + "FriendsTag: " + evt.sCreater + "\n";
            Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
            buf.append(data);
            buf.close();
            Toast.makeText(getApplicationContext(), "Bạn đã được mời một sự kiện bởi tài khoản " + evt.sCreater, Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Không thể mở file", Toast.LENGTH_LONG).show();
        }
    }
    private void CheckFriend()
    {
        new CheckFriendAsyncTask().execute(mainUser.sName);
    }

    public class CheckFriendAsyncTask extends AsyncTask<String, Void, ArrayList<FriendsData>>
    {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected ArrayList<FriendsData> doInBackground(String... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            ArrayList<FriendsData> arrFriend = new ArrayList<FriendsData>();
            arrFriend = CS.checkFriend(params[0]);
            return arrFriend;
        }
        @Override
        protected void onPostExecute(ArrayList<FriendsData> result)
        {
            super.onPostExecute(result);
            dialog.dismiss();
            SaveNewFriends(result);
        }
    }
    private void SaveNewFriends(ArrayList<FriendsData> arrFriends)
    {
        try
        {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            myFile = new File(newFolder + "/Friends.txt");
            if (!myFile.exists()) {
                boolean res = myFile.createNewFile();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile, true));

            for(FriendsData fData : arrFriends)
            {
                String data = "Name: " + fData.sName + "\n"
                        + "Address: " + fData.sAddress + "\n"
                        + "Phone: " + fData.sPhone + "\n";
                Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
                buf.append(data);
                Toast.makeText(getApplicationContext(),"Tài khoản " + fData.sName + " đã được thêm vào danh sách bạn bè",Toast.LENGTH_LONG).show();
            }

            buf.close();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Thêm bạn thất bại",Toast.LENGTH_LONG).show();
            finish();
        }
    }
    private void LoadUser() {
        int userfriendnumberOfProp = 3;
        mainUser = null;
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(this, "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return ;
                }
            }
            myFile = new File(newFolder + "/User.txt");
            if (myFile.exists()) {
                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
                String DataRow = "";
                int i = -1;
                User fus = new User();
                while ((DataRow = bufferedReader.readLine()) != null)
                {
                    i++;
                    switch ((i % userfriendnumberOfProp))
                    {
                        case 0:
                            fus.sName = DataRow;
                            break;
                        case 1:
                            fus.sPhone = DataRow;
                            break;
                        default:
                            fus.sAddress = DataRow;
                            mainUser = fus;
                    }
                }
                bufferedReader.close();
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Không thể mở file",Toast.LENGTH_LONG).show();
        }
        if(mainUser != null)
        {
            String cut[] = mainUser.sName.split(": ");
            mainUser.sName = cut[1];
            cut = mainUser.sAddress.split(": ");
            mainUser.sAddress = cut[1];
            cut = mainUser.sPhone.split(": ");
            mainUser.sPhone = cut[1];
        }
    }
    private void LoadData() {
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(this, "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return ;
                }
            }
            myFile = new File(newFolder + "/EventManager.txt");
            if (myFile.exists()) {
                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
                String DataRow = "";
                int i = -1;
                EventData edt = new EventData();
                while ((DataRow = bufferedReader.readLine()) != null)
                {
                    i++;
                    switch ((i % numberOfProp))
                    {
                        case 0:
                            edt.sEventId = DataRow;
                            break;
                        case 1:
                            edt.sTitle = DataRow;
                            break;
                        case 2:
                            edt.sInfo = DataRow;
                            break;
                        case 3:
                            edt.sDate = DataRow;
                            break;
                        case 4:
                            edt.sRemind_date = DataRow;
                            break;
                        case 5:
                            edt.sRemind_time = DataRow;
                            break;
                        case 6:
                            edt.sLocation = DataRow;
                            break;
                        default:
                            edt.sFriendsTag = DataRow;
                            data.add(edt);
                            edt = new EventData();
                    }
                }
                bufferedReader.close();
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Không thể mở file",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_add) {
            Intent myIntent=new Intent(MainActivity.this, AddEventActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("mainUser",mainUser);
            myIntent.putExtra("UserPacket",bundle);
            startActivityForResult(myIntent, REQUEST_CODE_INPUT_ADD_EVENT);
            return true;
        }
        if (id == R.id.action_friend) {
            Intent myIntent=new Intent(MainActivity.this, FriendsManageActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("mainUser",mainUser);
            myIntent.putExtra("UserPacket",bundle);
            startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode,resultCode,intent);
        if(requestCode == REQUEST_CODE_INPUT_ADD_EVENT)
        {
            switch (resultCode)
            {
                case RESULT_CODE_SAVE_EVENT :
                {
                    Bundle bundle = intent.getBundleExtra("EventPacket");
                    EventData eventData = (EventData)bundle.getSerializable("newEvent");
                    data.add(eventData);
                    customAdapter.notifyDataSetChanged();
                    break;
                }
                case RESULT_CODE_CANCEL:
                    break;
            }
        }
        if(requestCode == REQUEST_CODE_INPUT_EDIT_REMOVE)
        {
            switch (resultCode)
            {
                case RESULT_CODE_EDIT_EVENT :
                {
                    Bundle bundle = intent.getBundleExtra("EventPacket");
                    EventData eventData = (EventData)bundle.getSerializable("editEvent");
                    int position = (int)bundle.getInt("position");
                    data.set(position,eventData);
                    customAdapter.notifyDataSetChanged();
                    break;
                }
                case RESULT_CODE_DELETE_EVENT :
                {
                    Bundle bundle = intent.getBundleExtra("PositionPacket");
                    int position = (int)bundle.getInt("position");
                    data.remove(position);
                    customAdapter.notifyDataSetChanged();
                    break;
                }
                case RESULT_CODE_CANCEL:
                    break;
            }
        }
    }
}
