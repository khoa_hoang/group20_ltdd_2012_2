package khoa.moduleevent;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.widget.TextView;
import android.widget.Toast;

class callSoap {
    public FriendsData GetWeather(String cityname)
    {
        FriendsData fdata = new FriendsData();

        String SOAP_ACTION= "http://tempuri.org/findFriend";
        String OPERATION_NAME = "findFriend";
        String WDSL_TARGET_NAMESPACE ="http://tempuri.org/";

        String SOAP_ADDRESS = "http://eventmanager.somee.com/mywebservice.asmx";

        SoapObject request = new SoapObject(WDSL_TARGET_NAMESPACE,OPERATION_NAME);

        PropertyInfo PI = new PropertyInfo();
        PI.setName("user_name");
        PI.setValue(cityname);
        PI.setType(String.class);
        request.addProperty(PI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        try {
            HttpTransportSE httpTransportSE = new HttpTransportSE(SOAP_ADDRESS);
            httpTransportSE.setXmlVersionTag("<?xml version=\"1.0\" encoding= \"UTF-8\"?>");
            httpTransportSE.debug = true;
            httpTransportSE.call(SOAP_ACTION,envelope);

            SoapObject result=(SoapObject)envelope.getResponse();
            fdata.sName = result.getProperty("user_name").toString();
            fdata.sAddress = result.getProperty("address").toString();
            fdata.sPhone = result.getProperty("phone").toString();
        }
        catch (Exception e)
        {
            fdata.sName = null;
            e.printStackTrace();
        }
        return fdata;
    }
}
/*
public class FindFriendAsyncTask extends AsyncTask<String, Void, FriendsData> {

    FriendsData fdata = null;
    TextView txtV;
    FindFriendAsyncTask(TextView textView)
    {
        txtV = textView;
    }
    @Override
    protected FriendsData doInBackground(String... params)
    {

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final String URL="http://eventmanager.somee.com/mywebservice.asmx?WSDL";

        final String NAMESPACE="http://tempuri.org/";
        final String METHOD_NAME="findFriend";
        final String SOAP_ACTION=NAMESPACE+METHOD_NAME;

        try {

            SoapObject request=new SoapObject(NAMESPACE, METHOD_NAME);
            request.addProperty("user_name", params[0]);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet=true;
            envelope.setOutputSoapObject(request);

            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.call(SOAP_ACTION,envelope);

            SoapObject soapArray=(SoapObject) envelope.getResponse();

            for(int i=0; i<soapArray.getPropertyCount(); i++)
            {
                fdata = new FriendsData();
                //(SoapObject) soapArray.getProperty(i) get item at position i
                SoapObject soapItem =(SoapObject) soapArray.getProperty(i);
                //soapItem.getProperty("CateId") get value of CateId property
                //phải mapp đúng tên cột:
                fdata.sName = soapItem.getProperty("user_name").toString();
                fdata.sAddress = soapItem.getProperty("address").toString();
                fdata.sPhone = soapItem.getProperty("phone").toString();
            }
        }
        catch (Exception e)
        {

        }
        return fdata;
    }
    @Override
    protected void onPostExecute(FriendsData fdata)
    {
        if(fdata != null)
        {
            txtV.setText("Tài khoản có trên hệ thống");
        }
        else
        {
            txtV.setText("Tài khoản không tồn tại trên hệ thống");
        }
    }
}
*/
