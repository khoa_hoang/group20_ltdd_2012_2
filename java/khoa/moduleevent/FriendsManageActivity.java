package khoa.moduleevent;

import android.app.ActionBar;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class FriendsManageActivity extends ActionBarActivity {
    TextView name;
    TextView email;
    TextView phone;
    TextView address;
    User mainUser;

    ListView lstv = null;
    ArrayList<FriendsData> data = new ArrayList<FriendsData>();
    LoadFriendstoListView customAdapter = null;

    public static final int REQUEST_CODE_INPUT_ADD_FRIEND = 51;
    public static final int REQUEST_CODE_REGISTER_ACCOUNT = 52;
    public static final int REQUEST_CODE_MANAGE_ACCOUNT = 53;
    public static final int RESULT_CODE_ADD_FRIEND_SUCCESS=101;
    public static final int RESULT_CODE_CANCEL=102;
    public static final int RESULT_CODE_UNFRIEND=103;
    public static final int RESULT_CODE_REGISTER_SUCCESS=104;
    public static final int RESULT_CODE_EDIT_USER_INFO_SUCCESS=105;

    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friends_management);
        mainUser = new User();

        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("UserPacket");
        mainUser = (User)packageFromCaller.getSerializable("mainUser");

        LoadData();
        lstv = (ListView) findViewById(R.id.list);
        customAdapter = new LoadFriendstoListView(this,R.layout.friend_layout,data);
        lstv.setAdapter(customAdapter);

       /* lstv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(MainActivity.this, ShowDeleteEventActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putSerializable("editedEvent", data.get(position));
                myIntent.putExtra("dataPacket", bundle);
                startActivityForResult(myIntent, REQUEST_CODE_INPUT_EDIT_REMOVE);
            }
        });*/
    }


    private void LoadData() {
        int friendnumberOfProp = 3;
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(this, "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    return ;
                }
            }
            myFile = new File(newFolder + "/Friends.txt");
            if (myFile.exists()) {
                FileInputStream fIn = new FileInputStream(myFile);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
                String DataRow = "";
                int i = -1;
                FriendsData fdt = new FriendsData();
                while ((DataRow = bufferedReader.readLine()) != null)
                {
                    i++;
                    switch ((i % friendnumberOfProp))
                    {
                        case 0:
                            fdt.sName = DataRow;
                            break;
                        case 1:
                            fdt.sPhone = DataRow;
                            break;
                        default:
                            fdt.sAddress = DataRow;
                            data.add(fdt);
                            fdt = new FriendsData();
                    }
                }
                bufferedReader.close();
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Không thể mở file",Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_friend, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_friend) {
            if(mainUser == null)
            {
                Intent myIntent=new Intent(FriendsManageActivity.this, RegisterActivity.class);
                startActivityForResult(myIntent, REQUEST_CODE_REGISTER_ACCOUNT);
                return true;
            }
            else
            {
                Intent myIntent=new Intent(FriendsManageActivity.this, AddFriendActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("mainUser",mainUser);
                myIntent.putExtra("UserPacket",bundle);
                startActivityForResult(myIntent, REQUEST_CODE_INPUT_ADD_FRIEND);
                return true;
            }
        }
        if (id == R.id.action_account) {
            if(mainUser == null)
            {
                Intent myIntent=new Intent(FriendsManageActivity.this, RegisterActivity.class);
                startActivityForResult(myIntent, REQUEST_CODE_REGISTER_ACCOUNT);
                return true;
            }
            else
            {
                Intent myIntent=new Intent(FriendsManageActivity.this, UserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("mainUser",mainUser);
                myIntent.putExtra("UserPacket",bundle);
                startActivityForResult(myIntent, REQUEST_CODE_MANAGE_ACCOUNT);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode,resultCode,intent);

        if(requestCode == REQUEST_CODE_INPUT_ADD_FRIEND)
        {
            switch (resultCode)
            {
                case RESULT_CODE_ADD_FRIEND_SUCCESS :
                {
                    Bundle bundle = intent.getBundleExtra("FriendPacket");
                    FriendsData fdata = (FriendsData)bundle.getSerializable("newFriend");
                    data.add(fdata);
                    customAdapter.notifyDataSetChanged();
                    break;
                }
                case RESULT_CODE_CANCEL:
                    break;
            }
        }
        if(requestCode == REQUEST_CODE_REGISTER_ACCOUNT)
        {
            switch (resultCode)
            {
                case RESULT_CODE_REGISTER_SUCCESS :
                {
                    Bundle bundle = intent.getBundleExtra("UserPacket");
                    User user = (User)bundle.getSerializable("newUser");
                    mainUser = user;
                    break;
                }
                case RESULT_CODE_CANCEL:
                    break;
            }
        }
        if(requestCode == REQUEST_CODE_MANAGE_ACCOUNT)
        {
            switch (resultCode)
            {
                case RESULT_CODE_EDIT_USER_INFO_SUCCESS :
                {
                    Bundle bundle = intent.getBundleExtra("UserPacket");
                    User user = (User)bundle.getSerializable("infoUser");
                    mainUser = user;
                    break;
                }
                case RESULT_CODE_CANCEL:
                    break;
            }
        }
    }
}
