package khoa.moduleevent;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AddFriendActivity extends Activity {
    TextView tvSearchResult;
    EditText edName;
    Button btn_add_friend;
    Button btn_delete;
    Button btn_search;
    Button btn_cancel;
    User mainUser;

    FriendsData fData;
    public static final int RESULT_CODE_ADD_FRIEND_SUCCESS=101;
    public static final int RESULT_CODE_CANCEL=102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friend);

        tvSearchResult = (TextView)findViewById(R.id.search_result);
        edName = (EditText) findViewById(R.id.name);
        btn_add_friend = (Button) findViewById(R.id.add_friend_btn);
        btn_delete = (Button) findViewById(R.id.delete_btn);
        btn_search = (Button) findViewById(R.id.search_btn);
        btn_cancel = (Button) findViewById(R.id.cancel_btn);

        btn_delete.setVisibility(View.INVISIBLE);
        btn_add_friend.setVisibility(View.INVISIBLE);

        fData = new FriendsData();

        Intent callerIntent = getIntent();
        Bundle packageFromCaller = callerIntent.getBundleExtra("UserPacket");
        mainUser = (User)packageFromCaller.getSerializable("mainUser");

        // SEARCH
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FindFriendAsyncTask().execute(edName.getText().toString());
            }
        });
        // ADD
        btn_add_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // bug mainUser + edName
                String para = mainUser.sName + "///" + edName.getText().toString();
                new AddFriendAsyncTask().execute(para);
            }
        });
        // DELETE
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fData = null;
                btn_delete.setVisibility(View.INVISIBLE);
                btn_search.setVisibility(View.VISIBLE);
                btn_add_friend.setVisibility(View.INVISIBLE);
                edName.setFocusableInTouchMode(true);
                edName.setText("");
                tvSearchResult.setText("");
            }
        });
        // CANCEL
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToMain(RESULT_CODE_CANCEL, null);
            }
        });
    }

    private void SaveFriend()
    {
        try
        {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null);
                    return;
                }
            }
            myFile = new File(newFolder + "/Friends.txt");
            if (!myFile.exists()) {
                boolean res = myFile.createNewFile();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null);
                    return;
                }
            }
            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile, true));

            String data = "Name: " + fData.sName + "\n"
                    + "Address: " + fData.sAddress + "\n"
                    + "Phone: " + fData.sPhone + "\n";
            Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
            buf.append(data);
            buf.close();
            Toast.makeText(getApplicationContext(),"Tài khoản " + fData.sName + " đã được thêm vào danh sách bạn bè",Toast.LENGTH_LONG).show();
            sendToMain(RESULT_CODE_ADD_FRIEND_SUCCESS,fData);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Thêm bạn thất bại",Toast.LENGTH_LONG).show();
            finish();
        }
    }
    private void sendToMain(int resultcode, FriendsData fdata)
    {
        Intent intent = getIntent();
        if(fdata != null)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("newFriend",fdata);
            intent.putExtra("FriendPacket",bundle);
        }
        setResult(resultcode,intent);
        finish();
    }
    public class AddFriendAsyncTask extends AsyncTask<String, Void, String>
    {
        private final ProgressDialog dialog = new ProgressDialog(AddFriendActivity.this);

        @Override
        protected String doInBackground(String... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            String res = "";
            res = CS.AddFriend(params[0]);
            return res;
        }
        @Override
        protected void onPostExecute(String res)
        {
            super.onPostExecute(res);
            dialog.dismiss();

            if(res.equals("true"))
            {
                SaveFriend();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Thêm bạn không thành công",Toast.LENGTH_LONG).show();
            }
        }
    }
    public class FindFriendAsyncTask extends AsyncTask<String, Void, FriendsData>
    {
        private final ProgressDialog dialog = new ProgressDialog(AddFriendActivity.this);

        @Override
        protected FriendsData doInBackground(String... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            FriendsData fdata = new FriendsData();
            fdata = CS.GetFriend(params[0]);
            return fdata;
        }
        @Override
        protected void onPostExecute(FriendsData data)
        {
            super.onPostExecute(data);
            dialog.dismiss();
            if(data.sName == null)
            {
                String result = "Không tồn tại tài khoản trên hệ thống";
                tvSearchResult.setText(result);
                fData = null;
            }
            else
            {
                String result = "Người dùng: " + data.sName + "\nSố điện thoại: " + data.sPhone;
                tvSearchResult.setText(result);
                btn_delete.setVisibility(View.VISIBLE);
                btn_add_friend.setVisibility(View.VISIBLE);
                btn_search.setVisibility(View.INVISIBLE);
                edName.setFocusable(false);
                fData = data;
            }
        }
    }
}
