package khoa.moduleevent;


import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

class LoadDatatoListView extends ArrayAdapter {
    ArrayList<EventData> data = new ArrayList<EventData>();
    Context context;
    int layoutID;
    LoadDatatoListView(Context context, int layoutId,ArrayList<EventData> arrLst)
    {
        super(context, R.layout.event_layout,arrLst);
        this.layoutID = layoutId;
        this.context = context;
        this.data = arrLst;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View row = inflater.inflate(this.layoutID, null);
        if(data.size() > 0 && position >= 0)
        {
            TextView title_txt = (TextView) row.findViewById(R.id.title);
            TextView info_txt = (TextView) row.findViewById(R.id.info);
            TextView txt_time = (TextView) row.findViewById(R.id.time);
            TextView txt_location = (TextView) row.findViewById(R.id.location);

            title_txt.setText(data.get(position).sTitle);
            info_txt.setText(data.get(position).sInfo);
            txt_time.setText(data.get(position).sDate);
            txt_location.setText(data.get(position).sLocation);
        }
        return row;
    }


}
class EventData implements Serializable
{
    public String sEventId;
    public String sTitle;
    public String sInfo;
    public String sDate;
    public String sRemind_date;
    public String sRemind_time;
    public String sLocation;
    public String sFriendsTag;
    public String sCreater;

    EventData()
    {

    }

    EventData(String title, String info, String date, String remind_date, String remind_time, String location)
    {
        sTitle = title;
        sInfo = info;
        sDate = date;
        sRemind_date = remind_date;
        sRemind_time = remind_time;
        sLocation = location;
    }
}