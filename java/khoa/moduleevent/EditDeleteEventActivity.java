package khoa.moduleevent;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class EditDeleteEventActivity extends Activity {

    EditText edTitle;
    EditText edInfo;
    DatePicker dpDate;
    DatePicker dpDate_Remind;
    TimePicker tpTime_remind;
    EditText edLocation;
    Button btn_Edit;
    Button btn_delete;
    Button btn_Cancel;
    int numberOfProp = 8;
    User mainUser;
    int pos = -1;

    public static final int RESULT_CODE_EDIT_EVENT=103;
    public static final int RESULT_CODE_DELETE_EVENT=104;
    public static final int RESULT_CODE_CANCEL=102;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_delete_event);

        edTitle = (EditText) findViewById(R.id.title);
        edInfo = (EditText) findViewById(R.id.info);
        dpDate = (DatePicker) findViewById(R.id.date);
        dpDate_Remind = (DatePicker) findViewById(R.id.date_remind);
        tpTime_remind = (TimePicker) findViewById(R.id.time_remind);
        edLocation = (EditText) findViewById(R.id.location);
        btn_Edit = (Button) findViewById(R.id.ok_btn);
        btn_delete = (Button) findViewById(R.id.delete_btn);
        btn_Cancel = (Button) findViewById(R.id.cancel_btn);

        mainUser = new User();

        Intent callerInten = getIntent();
        Bundle bundle = callerInten.getBundleExtra("dataPacket");
        pos = bundle.getInt("position");
        final EventData chosenEvent = (EventData)bundle.getSerializable("editedEvent");
        mainUser = (User)bundle.getSerializable("mainUser");
        String[] saTitle = chosenEvent.sTitle.split(": ");
        edTitle.setText(saTitle[1]);
        String[] saInfo = chosenEvent.sInfo.split(": ");
        edInfo.setText(saInfo[1]);
        String[] date = chosenEvent.sDate.split("/");
        String[] day = date[0].split(": ");
        dpDate.updateDate(Integer.valueOf(date[2]),Integer.valueOf(date[1]),Integer.valueOf(day[1]));
        String[] rm_date = chosenEvent.sRemind_date.split("/");
        String[] day2 = rm_date[0].split(": ");
        dpDate_Remind.updateDate(Integer.valueOf(rm_date[2]),Integer.valueOf(rm_date[1]),Integer.valueOf(day2[1]));
        String rm_time[] = chosenEvent.sRemind_time.split("/");
        String time[] = rm_time[0].split(": ");
        tpTime_remind.setCurrentHour(Integer.valueOf(time[1]));
        tpTime_remind.setCurrentMinute(Integer.valueOf(rm_time[1]));
        String[] saLoca = chosenEvent.sLocation.split(": ");
        edLocation.setText(saLoca[1]);

        btn_Edit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    File myFile;
                    File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
                    if (!newFolder.exists()) {
                        boolean res = newFolder.mkdir();
                        if (!res) {
                            Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                            sendToMain(RESULT_CODE_CANCEL,null,-1);
                            return;
                        }
                    }
                    myFile = new File(newFolder + "/EventManager.txt");
                    if (!myFile.exists()) {
                        boolean res = myFile.createNewFile();
                        if (!res) {
                            Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                            sendToMain(RESULT_CODE_CANCEL,null,-1);
                            return;
                        }
                    }
                    FileInputStream fIn = new FileInputStream(myFile);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
                    String DataRow = "";
                    String DataFile = "";
                    int numLine = 0;
                    while ((DataRow = bufferedReader.readLine()) != null)
                    {
                        if((numLine >= (pos * numberOfProp)) && (numLine < ((pos + 1) * numberOfProp)))
                        {
                            switch ((numLine % numberOfProp))
                            {
                                case 0:
                                    DataRow = "Title: " + edTitle.getText().toString();
                                    break;
                                case 1:
                                    DataRow = "Information: " + edInfo.getText().toString();
                                    break;
                                case 2:
                                    DataRow = "Date: " + getStringDateFormat(dpDate);
                                    break;
                                case 3:
                                    DataRow = "Date remind: " + getStringDateFormat(dpDate_Remind);
                                    break;
                                case 4:
                                    DataRow = "Time remind: " + getStringTimeFormat(tpTime_remind);
                                    break;
                                default:
                                    DataRow = "Location: " + edLocation.getText().toString();
                            }
                        }
                        DataFile += DataRow + "\n";
                        numLine++;
                    }
                    bufferedReader.close();

                    BufferedWriter buf = new BufferedWriter(new FileWriter(myFile));
                    //Toast.makeText(getApplicationContext(), DataFile, Toast.LENGTH_LONG).show();
                    buf.append(DataFile);
                    buf.close();
                    String sTitle = edTitle.getText().toString();
                    String sInfo = edInfo.getText().toString();
                    String sLocation = edLocation.getText().toString();

                    Toast.makeText(getApplicationContext(), "Chỉnh sửa sự kiện thành công!", Toast.LENGTH_LONG).show();
                    EventData eventData = new EventData(sTitle,sInfo,getStringDateFormat(dpDate),getStringDateFormat(dpDate_Remind),getStringTimeFormat(tpTime_remind),sLocation);
                    sendToMain(RESULT_CODE_EDIT_EVENT,eventData,pos);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Không thể mở file", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null,-1);
                }
            }
        });
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> para = new ArrayList<String>();
                para.add(mainUser.sName);
                String[] arrStr = chosenEvent.sEventId.split(": ");
                para.add(arrStr[1]);
                new DeleteEventAsyncTask().execute(para);
            }
        });
        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public class DeleteEventAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
    {
        private final ProgressDialog dialog = new ProgressDialog(EditDeleteEventActivity.this);

        @Override
        protected String doInBackground(ArrayList<String>... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            String res = CS.deleteEvent(params[0].get(0),params[0].get(1));
            return res;
        }
        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            dialog.dismiss();
            if(result.equals("true"))
            {
                deleteEvent(pos);
                Toast.makeText(getApplicationContext(),"Xóa sự kiện thành công",Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(),result,Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),"Xóa sự kiện thất bại",Toast.LENGTH_LONG).show();
            }
        }
    }
    private void deleteEvent(int position)
    {
        try {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null,-1);
                    return;
                }
            }
            myFile = new File(newFolder + "/EventManager.txt");
            if (!myFile.exists()) {
                boolean res = myFile.createNewFile();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null,-1);
                    return;
                }
            }
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fIn));
            String DataRow = "";
            String DataFile = "";
            int numLine = 0;
            while ((DataRow = bufferedReader.readLine()) != null)
            {
                if((numLine >= (position * numberOfProp)) && (numLine < ((position + 1) * numberOfProp)))
                {
                    numLine++;
                    continue;
                }
                DataFile += DataRow + "\n";
                numLine++;
            }
            bufferedReader.close();

            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile));
            buf.append(DataFile);
            buf.close();


            Toast.makeText(getApplicationContext(), "Xóa sự kiện thành công!", Toast.LENGTH_LONG).show();
            sendToMain(RESULT_CODE_DELETE_EVENT,null,position);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Không thể mở file", Toast.LENGTH_LONG).show();
            sendToMain(RESULT_CODE_CANCEL,null,-1);
        }
    }
    private void sendToMain(int resultcode, EventData eventData, int position)
    {
        Intent intent = getIntent();
        if(eventData != null)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("editEvent",eventData);
            bundle.putInt("position",position);
            intent.putExtra("EventPacket",bundle);
        }
        if(resultcode == RESULT_CODE_DELETE_EVENT)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("position",position);
            intent.putExtra("PositionPacket",bundle);
        }
        setResult(resultcode,intent);
        finish();
    }
    private String getStringDateFormat(DatePicker dp)
    {
        int   day  = dp.getDayOfMonth();
        int   month= dp.getMonth();
        int   year = dp.getYear();
        return Integer.toString(day) + "/" + Integer.toString(month) + "/" + Integer.toString(year);
    }
    private String getStringTimeFormat(TimePicker dp)
    {
        int   hours  = dp.getCurrentHour();
        int   minutes= dp.getCurrentMinute();

        return Integer.toString(hours) + "/" + Integer.toString(minutes);
    }
}
