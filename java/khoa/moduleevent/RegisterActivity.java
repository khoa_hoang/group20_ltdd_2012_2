package khoa.moduleevent;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class RegisterActivity extends Activity {

    EditText edName;
    EditText edAddress;
    EditText edPhone;
    EditText edPassword;
    Button register_btn;
    Button cancel_btn;
    TextView tvresult;
    User mainUser;

    public static final int RESULT_CODE_CANCEL=102;
    public static final int RESULT_CODE_REGISTER_SUCCESS=104;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_user);

        edName = (EditText)findViewById(R.id.user_name);
        edAddress = (EditText)findViewById(R.id.address);
        edPhone = (EditText)findViewById(R.id.phone);
        edPassword = (EditText)findViewById(R.id.password);
        tvresult = (TextView)findViewById(R.id.result_webservice);
        register_btn = (Button)findViewById(R.id.register);
        cancel_btn = (Button)findViewById(R.id.cancel);


        mainUser = new User();

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvresult.setText("Xin vui lòng chờ trong giây lát...");
                mainUser.sName = edName.getText().toString();
                mainUser.sPhone = edPhone.getText().toString();
                mainUser.sAddress = edAddress.getText().toString();
                mainUser.sPassword = edPassword.getText().toString();
                new RegisterAsyncTask().execute(mainUser);
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToMain(RESULT_CODE_CANCEL,null);
            }
        });
    }
    public class RegisterAsyncTask extends AsyncTask<User, Void, String>
    {
        private final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);

        @Override
        protected String doInBackground(User... params)
        {
            CallSoapWebService CS = new CallSoapWebService();
            String res = "";
            res = CS.RegisAccount(params[0]);
            return res;
        }
        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            dialog.dismiss();
            if(result.equals("ok"))
            {
                SaveUser();
            }
            else
            {
                tvresult.setText(result);
            }
        }
    }
    private void SaveUser()
    {
        try
        {
            File myFile;
            File newFolder = new File(Environment.getExternalStorageDirectory(), "EventManager");
            if (!newFolder.exists()) {
                boolean res = newFolder.mkdir();
                if (!res) {
                    Toast.makeText(getApplicationContext(), "Không thể tạo thư mục", Toast.LENGTH_LONG).show();
                    sendToMain(RESULT_CODE_CANCEL,null);
                    return;
                }
            }
            myFile = new File(newFolder + "/User.txt");

            boolean res = myFile.createNewFile();
            if (!res) {
                Toast.makeText(getApplicationContext(), "Không thể tạo file", Toast.LENGTH_LONG).show();
                sendToMain(RESULT_CODE_CANCEL,null);
                return;
            }

            BufferedWriter buf = new BufferedWriter(new FileWriter(myFile, true));
            String data = "Name: " + mainUser.sName + "\n"
                    + "Address: " + mainUser.sAddress + "\n"
                    + "Phone: " + mainUser.sPhone + "\n";
            Toast.makeText(getApplicationContext(), data, Toast.LENGTH_LONG).show();
            buf.append(data);
            buf.close();
            Toast.makeText(getApplicationContext(),"Tài khoản " + mainUser.sName + " đã được tạo",Toast.LENGTH_LONG).show();
            sendToMain(RESULT_CODE_REGISTER_SUCCESS,mainUser);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),"Tạo tài khoản thất bại",Toast.LENGTH_LONG).show();
            finish();
        }
    }
    private void sendToMain(int resultcode, User us)
    {
        Intent intent = getIntent();
        if(us != null)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("newUser",us);
            intent.putExtra("UserPacket",bundle);
        }
        setResult(resultcode,intent);
        finish();
    }
}
