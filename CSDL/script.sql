USE [EventDatabase]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 6/7/2015 11:41:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[address] [nvarchar](150) NULL,
	[phone] [nvarchar](50) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Event]    Script Date: 6/7/2015 11:41:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[event_id] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](100) NULL,
	[information] [nvarchar](150) NULL,
	[date] [nvarchar](50) NULL,
	[time] [nvarchar](50) NULL,
	[location] [nvarchar](100) NULL,
	[creater] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Event_joiner]    Script Date: 6/7/2015 11:41:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event_joiner](
	[ej_id] [int] IDENTITY(1,1) NOT NULL,
	[event_id] [int] NOT NULL,
	[account_id] [int] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_Event_joiner] PRIMARY KEY CLUSTERED 
(
	[ej_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Friends]    Script Date: 6/7/2015 11:41:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Friends](
	[friend_id] [int] IDENTITY(1,1) NOT NULL,
	[friend1] [int] NOT NULL,
	[friend2] [int] NOT NULL,
	[isCheck] [int] NOT NULL,
 CONSTRAINT [PK_Friends] PRIMARY KEY CLUSTERED 
(
	[friend_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([account_id], [user_name], [password], [address], [phone]) VALUES (10, N'khoahoang', N'***', N'111 Nguyễn Văn Cừ', N'012345678')
INSERT [dbo].[Account] ([account_id], [user_name], [password], [address], [phone]) VALUES (11, N'minhkhoi', N'***', N'123 ákas', N'019191919191')
INSERT [dbo].[Account] ([account_id], [user_name], [password], [address], [phone]) VALUES (12, N'phuocan', N'***', N'135 Nguyễn Văn Cừ', N'0902646389')
SET IDENTITY_INSERT [dbo].[Account] OFF
SET IDENTITY_INSERT [dbo].[Event] ON 

INSERT [dbo].[Event] ([event_id], [title], [information], [date], [time], [location], [creater]) VALUES (6, N'di boi', N'iasdsdasf', N'22/12/2015', N'2:30', N'àdsadasd', N'khoahoang')
INSERT [dbo].[Event] ([event_id], [title], [information], [date], [time], [location], [creater]) VALUES (7, N'test', N'asfdasfa', N'22/12/2015', N'2/30', N'lasld', N'khoahoang')
INSERT [dbo].[Event] ([event_id], [title], [information], [date], [time], [location], [creater]) VALUES (8, N'test', N'asfdasfa', N'22/12/2015', N'2/30', N'lasld', N'khoahoang')
SET IDENTITY_INSERT [dbo].[Event] OFF
SET IDENTITY_INSERT [dbo].[Event_joiner] ON 

INSERT [dbo].[Event_joiner] ([ej_id], [event_id], [account_id], [status]) VALUES (11, 6, 10, 1)
INSERT [dbo].[Event_joiner] ([ej_id], [event_id], [account_id], [status]) VALUES (12, 6, 11, 0)
INSERT [dbo].[Event_joiner] ([ej_id], [event_id], [account_id], [status]) VALUES (13, 7, 10, 1)
INSERT [dbo].[Event_joiner] ([ej_id], [event_id], [account_id], [status]) VALUES (14, 7, 12, 1)
INSERT [dbo].[Event_joiner] ([ej_id], [event_id], [account_id], [status]) VALUES (15, 8, 10, -1)
INSERT [dbo].[Event_joiner] ([ej_id], [event_id], [account_id], [status]) VALUES (16, 8, 12, -1)
SET IDENTITY_INSERT [dbo].[Event_joiner] OFF
SET IDENTITY_INSERT [dbo].[Friends] ON 

INSERT [dbo].[Friends] ([friend_id], [friend1], [friend2], [isCheck]) VALUES (5, 10, 12, 0)
INSERT [dbo].[Friends] ([friend_id], [friend1], [friend2], [isCheck]) VALUES (6, 10, 11, 0)
SET IDENTITY_INSERT [dbo].[Friends] OFF
ALTER TABLE [dbo].[Event_joiner]  WITH CHECK ADD  CONSTRAINT [FK_Event_joiner_Account] FOREIGN KEY([account_id])
REFERENCES [dbo].[Account] ([account_id])
GO
ALTER TABLE [dbo].[Event_joiner] CHECK CONSTRAINT [FK_Event_joiner_Account]
GO
ALTER TABLE [dbo].[Event_joiner]  WITH CHECK ADD  CONSTRAINT [FK_Event_joiner_Event] FOREIGN KEY([event_id])
REFERENCES [dbo].[Event] ([event_id])
GO
ALTER TABLE [dbo].[Event_joiner] CHECK CONSTRAINT [FK_Event_joiner_Event]
GO
ALTER TABLE [dbo].[Friends]  WITH CHECK ADD  CONSTRAINT [FK_Friends_Account] FOREIGN KEY([friend1])
REFERENCES [dbo].[Account] ([account_id])
GO
ALTER TABLE [dbo].[Friends] CHECK CONSTRAINT [FK_Friends_Account]
GO
ALTER TABLE [dbo].[Friends]  WITH CHECK ADD  CONSTRAINT [FK_Friends_Account1] FOREIGN KEY([friend2])
REFERENCES [dbo].[Account] ([account_id])
GO
ALTER TABLE [dbo].[Friends] CHECK CONSTRAINT [FK_Friends_Account1]
GO
