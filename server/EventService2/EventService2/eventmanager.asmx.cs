﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace EventService2
{
    /// <summary>
    /// Summary description for eventmanager
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eventmanager : System.Web.Services.WebService
    {
        DataClasses1DataContext db = null;
        public eventmanager()
        {
            db = new DataClasses1DataContext();
        }
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public Account findFriend(string user_name)
        {
            Account fr = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (fr != null)
            {
                fr.Event_joiners.Clear();
                fr.Friends.Clear();
                fr.password = "";
                fr.Friends1.Clear();
                return fr;
            }
            return null;
        }
        [WebMethod]
        public bool addAccount(string user_name, string address, string phone, string password)
        {
            Account p = null;
            p = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (p == null)
            {
                try
                {
                    Account acc = new Account();
                    acc.user_name = user_name;
                    acc.address = address;
                    acc.phone = phone;
                    acc.password = password;
                    db.Accounts.InsertOnSubmit(acc);
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        [WebMethod]
        public bool editInfo(string user_name, string address, string phone, string password)
        {
            Account p = null;
            p = db.Accounts.FirstOrDefault(x => x.user_name == user_name);

            if ((p == null) || (!p.password.Equals(password)))
            {
                return false;
            }
            else
            {
                try
                {
                    p.address = address;
                    p.phone = phone;
                    db.SubmitChanges();
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return true;
        }
        [WebMethod]
        public bool addFriend(string information)
        {
            string[] stringSeparators = new string[] { "///" };
            string[] friends = information.Split(stringSeparators, StringSplitOptions.None);
            Account fr1 = db.Accounts.FirstOrDefault(x => x.user_name == friends[0]);
            Account fr2 = db.Accounts.FirstOrDefault(x => x.user_name == friends[1]);
            
            if ((fr1 == null) || (fr2 == null))
                return false;
            else
            {            
                Friend f1 = db.Friends.FirstOrDefault(x => ((x.friend1 == fr1.account_id) && (x.friend2 == fr2.account_id)));
                Friend f2 = db.Friends.FirstOrDefault(x => ((x.friend1 == fr2.account_id) && (x.friend2 == fr1.account_id)));
                if ((f1 != null) || (f2 != null))
                {
                    return false;
                }
                else
                {
                    try
                    {
                        Friend f = new Friend();
                        f.friend1 = fr1.account_id;
                        f.friend2 = fr2.account_id;
                        f.isCheck = 0;
                        db.Friends.InsertOnSubmit(f);
                        db.SubmitChanges();
                        return true;
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
            }
        }
        [WebMethod]
        public bool ResetFriend()
        {
            List<Friend> lst = db.Friends.Where(x => x.friend_id > 0).ToList();
            foreach (Friend fr in lst)
            {
                db.Friends.DeleteOnSubmit(fr);
                db.SubmitChanges();
            }
            return true;
        }
        [WebMethod]
        public bool ResetEvent()
        {
            List<Event_joiner> lst = db.Event_joiners.Where(x => x.ej_id > 0).ToList();
            foreach (Event_joiner ej in lst)
            {
                db.Event_joiners.DeleteOnSubmit(ej);
                db.SubmitChanges();
            }
            List<Event> lst_evt = db.Events.Where(x => x.event_id > 0).ToList();
            foreach (Event ev in lst_evt)
            {
                db.Events.DeleteOnSubmit(ev);
                db.SubmitChanges();
            }
            return true;
        }
        [WebMethod]
        public List<Account> checkFriend(string user_name)
        {
            List<Account> lstacc = new List<Account>();
            Account thisAcc = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (thisAcc == null)
            {
                return null;
            }
            List<Friend> lstFriend = db.Friends.Where(x => x.friend2 == thisAcc.account_id && x.isCheck == 0).ToList();
            foreach (Friend fr in lstFriend)
            {
                Friend temp = db.Friends.FirstOrDefault(x => x.friend_id == fr.friend_id);
                temp.isCheck = 1;                
            }
            db.SubmitChanges();
            foreach (Friend fr in lstFriend)
            {    
                Account fracc = db.Accounts.FirstOrDefault(x => x.account_id == fr.friend1);
                fracc.Event_joiners.Clear();
                fracc.Friends.Clear();
                fracc.password = "";
                lstacc.Add(fracc);
            }
            return lstacc;
        }
        [WebMethod]
        public int addEvent(string title, string information, string date, string time,string location, string creator, string friendtags)
        {
            Account cre = db.Accounts.FirstOrDefault(x => x.user_name == creator);
            if (cre == null)
            {
                return -1;
            }
            try
            {
                Event ev = new Event();
                ev.title = title;
                ev.information = information;
                ev.date = date;
                ev.time = time;
                ev.location = location;
                ev.creater = cre.user_name;

                db.Events.InsertOnSubmit(ev);
                db.SubmitChanges();

                string[] stringSeparators = new string[] { "," };
                string[] ftag = friendtags.Split(stringSeparators, StringSplitOptions.None);

                Event_joiner EJ = new Event_joiner();
                EJ.event_id = ev.event_id;
                EJ.account_id = cre.account_id;
                EJ.status = 1;

                db.Event_joiners.InsertOnSubmit(EJ);
                db.SubmitChanges();

                foreach (string tag in ftag)
                {
                    Account fr = db.Accounts.FirstOrDefault(x => x.user_name == tag);
                    if (fr != null)
                    {
                        Event_joiner ej = new Event_joiner();
                        ej.event_id = ev.event_id;
                        ej.account_id = fr.account_id;
                        ej.status = 0;

                        db.Event_joiners.InsertOnSubmit(ej);
                        db.SubmitChanges();
                    }
                }
                return ev.event_id;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        [WebMethod]
        public List<Event> checkNewEvent(string user_name)
        {
            List<Event> lstevt = new List<Event>();
            Account thisAcc = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (thisAcc == null)
            {
                return null;
            }
            List<Event_joiner> lst_ej = db.Event_joiners.Where(x => x.account_id == thisAcc.account_id && x.status == 0).ToList();
            foreach (Event_joiner ej in lst_ej)
            {
                ej.status = 1;
                db.SubmitChanges();
            }

            foreach (Event_joiner ej in lst_ej)
            {
                Event evt = db.Events.FirstOrDefault(x => x.event_id == ej.event_id);
                evt.Event_joiners = null;
                lstevt.Add(evt);
            }
            return lstevt;
        }
        [WebMethod]
        public bool deleteEvent(string user_name, string event_id)
        {
            int ev_id = int.Parse(event_id);
            Account thisAcc = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (thisAcc == null)
            {
                return false;
            }
            Event_joiner ej = db.Event_joiners.FirstOrDefault(x => x.account_id == thisAcc.account_id && x.event_id == ev_id);
            if (ej == null)
            {
                return false;
            }

            try
            {
                db.Event_joiners.DeleteOnSubmit(ej);
                db.SubmitChanges();
            }
            catch (Exception e)
            {
                return false;
            }

            Event evt = db.Events.FirstOrDefault(x => x.event_id == ev_id);
            if (evt.creater == user_name)
            {
                List<Event_joiner> lst_ej = db.Event_joiners.Where(x => x.event_id == ev_id).ToList();
                try
                {
                    foreach (Event_joiner evj in lst_ej)
                    {
                        evj.status = -1;
                        db.SubmitChanges();
                    }
                }
                catch (Exception e)
                {
                    return false;
                }                
            }
            return true;
        }
        [WebMethod]
        public List<Event> checkDeleteEvent(string user_name)
        {
            List<Event> lstevt = new List<Event>();
            Account thisAcc = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (thisAcc == null)
            {
                return null;
            }
            List<Event_joiner> lst_ej = db.Event_joiners.Where(x => x.account_id == thisAcc.account_id && x.status == -1).ToList();
            foreach (Event_joiner ej in lst_ej)
            {
                ej.status = 1;
                db.SubmitChanges();
            }

            foreach (Event_joiner ej in lst_ej)
            {
                Event evt = db.Events.FirstOrDefault(x => x.event_id == ej.event_id);
                evt.Event_joiners = null;
                lstevt.Add(evt);
            }
            return lstevt;
        }
        [WebMethod]
        public List<Event> checkUpdateEvent(string user_name)
        {
            List<Event> lstevt = new List<Event>();
            Account thisAcc = db.Accounts.FirstOrDefault(x => x.user_name == user_name);
            if (thisAcc == null)
            {
                return null;
            }
            List<Event_joiner> lst_ej = db.Event_joiners.Where(x => x.account_id == thisAcc.account_id && x.status == 2).ToList();
            foreach (Event_joiner ej in lst_ej)
            {
                ej.status = 1;
                db.SubmitChanges();
            }

            foreach (Event_joiner ej in lst_ej)
            {
                Event evt = db.Events.FirstOrDefault(x => x.event_id == ej.event_id);
                evt.Event_joiners = null;
                lstevt.Add(evt);
            }
            return lstevt;
        }
    }
}
